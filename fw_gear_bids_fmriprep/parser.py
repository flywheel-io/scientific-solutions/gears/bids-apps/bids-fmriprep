"""Parser module to parse gear config.json."""

import logging
import os
from collections import defaultdict
from pathlib import Path
from typing import Dict, Tuple

from flywheel_bids.flywheel_bids_app_toolkit import BIDSAppContext
from flywheel_bids.flywheel_bids_app_toolkit.compression import unzip_archive_files
from flywheel_gear_toolkit import GearToolkitContext

from .utils.helpers import reconcile_existing_and_unzipped_files, reset_FS_subj_paths

log = logging.getLogger(__name__)


# This function mainly parses gear_context's config.json file and returns relevant
# inputs and options.
def parse_config(gear_context: GearToolkitContext) -> Tuple[bool, Dict]:
    """Search config for extra settings not used by BIDSAppContext.

    Args:
        gear_context (GearToolkitContext):

    Returns:
        debug (bool): Level of gear verbosity
        config_options (Dict): other config options explicitly called out
                in the manifest, not encapsulated by the bids_app_command
                field. The bids_app_command field is intended to handle the
                majority of the use cases to run the BIDS app.
    """

    debug = gear_context.config.get("debug")
    config_options = {}
    for key in gear_context.config.keys():
        # If there are extra kwargs that were highlighted in the Config UI
        # by putting them in the manifest specifically, add them to the list
        # to exclude (i.e., after "bids_app_command")
        if not key.startswith("gear-") and key not in ["debug", "bids_app_command", "previous_results"]:
            config_options[key] = gear_context.config.get(key)

    return debug, config_options


def parse_input_files(gear_context: GearToolkitContext, app_context: BIDSAppContext):
    """
    Fetch the input files from the manifest and return the filepaths.

    Grab the manifest blob for inputs. Refine to only file inputs to be included in
    the BIDS App command. The method will create a key-value pair entry for an
    input_files dictionary. Best practice design will be to use the kwarg in the
    BIDS App command as the input file label (key).

    Create exclusion criteria for input files that are not to be included for the
    BIDS App command.
    """
    inputs = gear_context.manifest.get("inputs")
    input_files = defaultdict()
    # Filter the context input file keys to use in the BIDS App command
    # (These are kwargs in BIDS fMRIPrep)
    include_keys = ["fs-subject-dir", "bids-filter-file"]
    if inputs:
        remove_keys = []
        for i in inputs:
            if i in include_keys and inputs[i]["base"] == "file" and gear_context.get_input_path(i):
                input_files[i] = gear_context.get_input_path(i)
                if "zip" in Path(input_files[i]).suffix:
                    input_files[i] = unzip_archive_files(gear_context, i + "_unzip")
                remove_keys.append(i)

        # Process other input files
        for i in [ii for ii in inputs.keys() if ii not in remove_keys]:
            if inputs[i]["base"] == "file" and gear_context.get_input_path(i):
                input_path = gear_context.get_input_path(i)
                if "zip" in Path(input_path).suffix:
                    # Unzip into work, rather than inputs to be compatible with HPC
                    unzip_dir = unzip_archive_files(gear_context, i, Path(gear_context.work_dir / i))
                    # Add other clean up methods. Is the destination ID still in the
                    # folder path?
                    if i == "previous_results":
                        reconcile_existing_and_unzipped_files(app_context.analysis_output_dir, unzip_dir)
    # Field specific methods
    if "fs-subject-dir" in gear_context.manifest.get("inputs"):
        reset_FS_subj_paths(gear_context)
        reconcile_existing_and_unzipped_files(os.environ["SUBJECTS_DIR"])
        input_files["fs-subject-dir"] = os.environ["SUBJECTS_DIR"]

    return input_files
