"""Small methods specific to this BIDS app gear."""

import logging
import os
import shutil
from pathlib import Path

from flywheel_bids.flywheel_bids_app_toolkit.commands import validate_kwargs
from flywheel_gear_toolkit import GearToolkitContext

log = logging.getLogger(__name__)


def pare_down_files(gear_context, app_context):
    """
    Remove extraneous files to limit the overall size of the zip.

    Currently, remove all:
        1) fsaverage* directories
    """
    if not gear_context.config.get("gear-keep-fsaverage"):
        fs_path = Path(app_context.analysis_output_dir).joinpath("freesurfer")
        fsavg_dirs = fs_path.glob("fsaverage*")
        for fsavg in fsavg_dirs:
            log.info("deleting %s", str(fsavg))
            shutil.rmtree(fsavg)
    else:
        log.info("Keeping fsaverage directories")


def reconcile_existing_and_unzipped_files(target_dir: Path, unzip_dir: Path):
    """Place unzipped files in the proper directory.

    Often archived/zipped inputs are unzipped into /flywheel/v0/work. Depending on the
    intended destination and whether extraneous enclosing folders exist (i.e., destination IDs),
    this method can be used to simply and consistently relocate files and/or directories to
    their intended, final resting places.

    Args:
        target_dir (Path): final, resting place for the dir or file
        unzip_dir (Path): initial location of the unzipped archive (often generated by GTK.utils.zip_tools.unzip_archive)
    """
    for file in unzip_dir.glob("*/*"):
        if (target_dir / file.name).exists():
            log.info("Found %s but using existing", file.name)
        else:
            if not target_dir.exists():
                target_dir.mkdir(parents=True)
            log.debug("Moving %s to %s", file.name, target_dir / file.name)
            shutil.move(file, target_dir / file.name)


def reset_FS_subj_paths(gear_context: GearToolkitContext):
    """Update pointers to the writable locations of the FreeSurfer SUBJECTS_DIR

    To increase HPC-compatibility, change the SUBJECTS_DIR to a writable location (i.e., /flywheel/v0/work) and update pointers for all the averages folders to use the writable location.

    Args:
        gear_context (GearToolkitContext): _description_
    """
    # All writeable directories need to be set up in the current working directory
    orig_subject_dir = Path(os.environ.get("SUBJECTS_DIR", "/opt/freesurfer/subjects"))
    subjects_dir = Path(gear_context.work_dir) / "freesurfer/subjects"
    # Send the new path back to the env
    os.environ["SUBJECTS_DIR"] = str(subjects_dir)
    if not subjects_dir.exists():  # needs to be created unless testing
        subjects_dir.mkdir(parents=True)
        (subjects_dir / "fsaverage").symlink_to(orig_subject_dir / "fsaverage")
        (subjects_dir / "fsaverage5").symlink_to(orig_subject_dir / "fsaverage5")
        (subjects_dir / "fsaverage6").symlink_to(orig_subject_dir / "fsaverage6")


def validate_setup(app_context):
    """Customizable validation pipeline for gear-dependent configuration options.

    The goal is to cause the gear to fail quickly and with useful debugging help
    prior to downloading BIDS data or running (doomed) BIDS algorithms.

    Validating the bids_app_options kwargs should be included for all BIDS App gears.
    Other items to consider for validation include:
    1) Do any input files require other input files?
    2) Do other modification scripts/methods need to be run on inputs?
    3) Are any config settings mutually exclusive and need to be double-checked?
    """
    if app_context.bids_app_options:
        validate_kwargs(app_context)
