# Release Notes

## 1.5.4_24.1.1

- Use latest BIDS Client, 1.2.26
- Updated to work on HPC

## 1.5.1_24.1.1

- Update to the latest [fMRIPrep release](https://fmriprep.org/en/stable/changes.html#october-10-2024)

ENH:

- Add autoupdate method in utils and associated test files.
- Updated documentation links.

## 1.5.0_23.2.1

Significant shift in configuration options.

- To increase maintainability and decrease dev cycle time for new releases, the
  command line arguments for BIDS fMRIPrep are no longer exposed individually in
  the configuration tab when running the gear.
- Please use the `bids_app_command` field in the config tab as you would use a
  terminal to enter the BIDS fMRIPrep command. _Recommendation_: copy + paste
  the fMRIPrep command you would type in your terminal/notebook file directly
  into the `bids_app_command` field.
- This change is anticipated to reduce errors, since it is easier and simpler
  than setting all the individual configuration options for a given run.

ENH

- This version is a rewrite of the gear that uses the 2024 BIDS App template and
  toolkit within Flywheel's BIDS tools. Some advanced features (including full
  HPC-compatibility) will be forthcoming as minor and patch revisions are
  released.
