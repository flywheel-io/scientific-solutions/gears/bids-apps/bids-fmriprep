#################################################################
# Develop the venv for Flywheel (flypy) in a modern python image.
# Older versions of Ubuntu, used by some BIDS Apps, are not
# compatible with modern pip. The mismatch headache can be
# avoided by creating a venv and copying the venv to the old
# Ubuntu base.
##################################################################
FROM flywheel/python:3.12-debian AS fw_base
WORKDIR ${FLYWHEEL}
ENV FLYWHEEL="/flywheel/v0"

# Install build dependencies
RUN apt-get update -yq && \
    apt-get upgrade -yq && \
    apt-get install --no-install-recommends -yq \
    git \
    build-essential \
    zip \
    nodejs \
    tree \
    curl \
    libssl-dev \
    zlib1g-dev \
    libncurses5-dev \
    libncursesw5-dev \
    libreadline-dev \
    libsqlite3-dev \
    libgdbm-dev \
    libdb5.3-dev \
    libbz2-dev \
    libexpat1-dev \
    liblzma-dev \
    libffi-dev \
    uuid-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Create and activate virtual environment
RUN python -m venv /opt/flypy --copies
ENV PATH="/opt/flypy/bin:$PATH"

# Copy requirements and install dependencies
COPY requirements.txt $FLYWHEEL/
RUN pip install -U --no-cache-dir pip && \
    pip install --no-cache-dir -r $FLYWHEEL/requirements.txt

# List installed packages
RUN pip freeze > /opt/flypy/installed_packages.txt

# Copy the application code
COPY ./ $FLYWHEEL/
RUN pip install --no-cache-dir $FLYWHEEL

# Isolate the versions of the dependencies within the BIDS App
# from the (potentially updated) Flywheel dependencies by copying
# the venv with the pip installed Flywheel deps.
FROM nipreps/fmriprep:24.1.1 AS bids_runner

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

# Copy the virtual environment from fw_base
COPY --from=fw_base /opt/flypy /opt/flypy
COPY --from=fw_base /usr/local /usr/local

# Update the softlink to point to fw_base's version of python in bids_runner
RUN ln -sf /usr/local/bin/python3.12 /opt/flypy/bin/python

# Copy the application code
COPY ./ $FLYWHEEL/

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["/opt/flypy/bin/python", "/flywheel/v0/run.py"]
