# BIDS fMRIPrep (bids-fmriprep)

# Update the BIDS algorithm version

The preferred method is to edit and run .utils.autoupdate and .tests.platform_test (see
global vars at the top). The main phases of the autoupdate script are captured in the
steps below, which one may run manually. The platform_test is part of the script and
attempts to run the newly built gear with minimal inputs.

1. Fork the repo.
2. [Update the DockerHub image](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-fmriprep/-/blob/main/Dockerfile?ref_type=heads#L65) that the gear will use.
3. Update the [gear-builder line](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-fmriprep/-/blob/main/manifest.json?ref_type=heads#L89) in the manifest.
4. Update the [version line](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-fmriprep/-/blob/main/manifest.json?ref_type=heads#L203) in the manifest.
5. Run `poetry update` from the local commandline (where your cwd is the top-level of the gear). This command will update any dependencies for the Flywheel portion of the gear (not the BIDS algorithm itself!).
6. Run `fw-beta gear build` to update anything in the manifest.
7. Ideally, run `fw-beta gear upload` and complete a test run on your Flywheel instance.
8. Run `git checkout -b {my-update}`, `git commit -a -m "{My message about updating}" -n`, and `git push`.
9. Submit a merge request (MR) to the original gear repo for Flywheel staff to review for official inclusion in the exchange.

## Overview

[Flywheel Gear](https://github.com/flywheel-io/gears/tree/master/spec) which runs [fMRIPrep](http://fmriprep.readthedocs.io) on BIDS-curated data. fMRIPrep is a functional magnetic resonance imaging (fMRI) data preprocessing pipeline that is designed to provide an easily accessible, state-of-the-art interface that is robust to variations in scan acquisition protocols and that requires minimal user input, while providing easily interpretable and comprehensive error and output reporting. It performs basic processing steps (coregistration, normalization, unwarping, noise component extraction, segmentation, skull stripping, etc.) providing outputs that can be easily submitted to a variety of group level analyses, including task-based or resting-state fMRI, graph theory measures, surface or volume-based statistics, etc.

The version number is (Flywheel gear) MAJOR . MINOR . PATCH \_ (algorithm) YY . MINOR . PATCH

This gear can only be run on datasets that have been BIDS curated and can pass the tests of the [BIDS Validator](https://github.com/bids-standard/bids-validator). fMRIPrep requires that your dataset include at least one T1w structural image. It can include multiple T1 images, a T2 image and some number of BOLD series. This data must have its DICOMS classified with our classifier gear, and converted to NIfTI files with our dcm2niix gear, in that order. There are additional requirements as described under "Troubleshooting" below.

This Gear requires a (free) Freesurfer license. The license can be provided to the Gear in 3 ways. See [How to include a Freesurfer license file](https://docs.flywheel.io/user/compute/gears/freesurfer/user_including_a_freesurfer_license_file_to_run_a_freesurfer_or_fmriprep_gear/).

The bids-fmriprep Gear can run at the subject or session level. Because files are in the BIDS format, all the proper files will be used for the given session, subject, or separately, by subject, for the whole project.

[Usage](#usage)

[FAQ](#faq)

### Summary

fMRIPrep 24.x.y is a functional magnetic resonance imaging (fMRI) data preprocessing pipeline that is designed to provide an easily accessible, state-of-the-art interface that is robust to variations in scan acquisition protocols and that requires minimal user input, while providing easily interpretable and comprehensive error and output reporting. It performs basic processing steps (coregistration, normalization, unwarping, noise component extraction, segmentation, skullstripping etc.) providing outputs that can be easily submitted to a variety of group level analyses, including task-based or resting-state fMRI, graph theory measures, surface or volume-based statistics, etc.

### Cite

https://fmriprep.org/en/stable/index.html#citation

### License

LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsl/docs/#/license for more information.

Gear: BSD-3-Clause
fMRIPrep: Apache 2.0
FSL: Non-commercial, see above
ANTS: Apache 2.0
AFNI: GNU GPL
FreeSurfer: GNU GPL + license file from MGH

### Classification

_Category:_ Analysis

_Gear Level:_
Note: Running from the project level will download ALL participant directories. The `fmriprep bids_dir output_dir participant` command will automatically be used as no "group" option is available.

- [x] Project (not recommended)
- [x] Subject
- [x] Session
- [ ] Acquisition
- [ ] Analysis

---

[[_TOC_]]

---

### Inputs

- previous_results

  - OPTIONAL
  - **Type**: file
  - If there are other files that will allow the algorithm to run or pick up analysis from a certain point, then the archive can be supplied here. This zip will be automatically unzipped within the gear. NOTE: When the BIDSAppContext is created, if this field is present (as "previous_results"), the toolkit will unzip the attached file into the BIDS directory and update paths appropriately. See BIDSAppContext.check_archived_inputs for more details.

- bids-filter-file

  - OPTIONAL
  - **Type**: file
  - Per the [kwargs for fmriprep](https://fmriprep.org/en/stable/usage.html#options-for-filtering-bids-queries), submit a JSON file describing custom BIDS files for PyBIDS.

- bidsignore

  - OPTIONAL
  - **Type**: file
  - Provide a .bidsignore file to pass to the bids-validator

- derivatives

  - OPTIONAL
  - **Type**: file
  - If there are other files that will allow the algorithm to run or pick up analysis from a certain point, then the archive can be supplied here. This zip will be automatically unzipped within the gear. NOTE: When the BIDSAppContext is created, if this field is present (as "derivatives"), the toolkit will unzip the attached file into the BIDS directory and update paths appropriately. See BIDSAppContext.check_derivatives for more details.

- freesurfer_license

  - OPTIONAL
  - **Type**: file
  - Populate the license key here or on the project to be able to use FreeSurfer. BIDSAppToolkit finds and copies the license to the $FREESURFER_HOME location.

- fs-subject-dir
  - OPTIONAL
  - **Type**: file
  - In line with the [kwargs allowed by fmriprep](https://fmriprep.org/en/stable/usage.html#specific-options-for-freesurfer-preprocessing), upload existing FreeSurfer subject directories.

### Config

- bids_app_command
  - OPTIONAL
  - **Type**: free-text
  - The gear will run the defaults for the algorithm without anything in this box. If you wish to provide the command as you would on a CLI, input the exact command here. Flywheel will automatically update the BIDS_dir, output_dir, and analysis_level. (e.g., `fmriprep bids_dir output participant --valid-arg1 --valid-arg2`)
  - For more help to build the command, please see [fmriPrep Usage](https://fmriprep.org/en/stable/usage.html#command-line-arguments).
  - Note: If you use a kwarg here, don't worry about putting a value in the box for the same kwarg below. Any kwarg that you see called out in the config UI and that is given a value will supersede the kwarg:value given as part of the bids_app_command.
- debug

  - **Type**: Boolean
  - **Default**: false
  - Verbosity of log messages; default results in INFO level, True will log DEBUG level

- gear-dry-run

  - **Type**: Boolean
  - **Default**: false (set to true for template)
  - Do everything related to Flywheel except actually execute BIDS App command.
  - Note: This is NOT the same as running the BIDS app command with `--dry-run`. gear-dry-run will not actually download the BIDS data, attempt to run the BIDS app command, or do any metadata/result updating.

- gear-post-processing-only

  - **Type**: Boolean
  - **Default**: false
  - If an archive file is available, one can pick up with a previously analyzed dataset. This option is useful if the metadata changed (or needs to change). It is also useful for development.

- gear-intermediate-files
  - **Type**: string
  - Space separated list of FILES to retain from the intermediate work directory.
- gear-intermediate-folders
  - **Type**: string
  - Space separated list of FOLDERS to retain from the intermediate work directory.
- gear-expose-all-outputs
  - **Type**: Boolean
  - **Default**: false
  - Keep ALL the extra output files that are created during the run in addition to the normal, zipped output. Note: This option may cause a gear failure because there are too many files for the engine.
- gear-save-intermediate-output
  - **Type**: Boolean
  - **Default**: false
  - Gear will save ALL intermediate output into fmriprep_work.zip

### Outputs

#### Files

- bids-fmriprep*{subj}*{destination_id}.zip

  - Full output of the fMRIPrep algorithm (except intermediate steps) in BIDS format

- bids_tree.html

  - Print out of structure of the BIDS _input_ data
  - Extra information about where the analysis was launched and which files were downloaded (The report from `export_bids` on Flywheel)

- job-{destination_id}.log

  - Same information that can be viewed under the "Log" tab in the Jobs Log window

- sub-{subj}.html.zip
  - Viewable summary report with QC snapshots and error reports

#### Metadata

No metadata is updated by this gear.

### Pre-requisites

#### Prerequisite Gear Runs

BIDS-curate must run successfully in order to fill in entities in file.info.BIDS. These fields are required for any BIDS app gear to be able to download the proper data.

#### Prerequisite Files

1. Fieldmaps ([BIDS specification](https://bids-specification.readthedocs.io/en/stable/04-modality-specific-files/01-magnetic-resonance-imaging-data.html#fieldmap-data))

- Phase encoding directions for fieldmaps and bold must be opposite.

  - Look for the `PhaseEncodingDirection` key in the file metadata (or exported JSON sidecar) and note the value (e.g., `j-`). If the fieldmap and the bold series it is `IntendedFor` do not have opposite `PhaseEncodingDirection`, fMRIPrep will error out with "ValueError: None of the discovered fieldmaps has the right phase encoding direction." BIDS specification details can be found [here](https://bids-specification.readthedocs.io/en/stable/04-modality-specific-files/01-magnetic-resonance-imaging-data.html#case-4-multiple-phase-encoded-directions-pepolar). For more information see discussions [here](https://github.com/nipreps/fmriprep/issues/1148#issuecomment-392363308) and [here](https://neurostars.org/t/phase-encoding-error-for-field-maps/2650).
  - To fix this, you can either write in "fieldmaps" into the "ignore" configuration option for the bids-fmriprep Gear, which will ignore using fieldmaps during the fMRIPrep workflow, or if you collected your data with opposite phase encodings, you can update the values for the `PhaseEncodingDirection` key as appropriate.

- `IntendedFor` field needs to point to an existing file.
  - There are two places where the `IntendedFor` file metadata is required for bids-fmriprep.
  - The structure of the metadata in Flywheel should be similar to the following:

```json
{
  "BIDS": {
    "Acq": "",
    "Dir": "",
    "error_message": "",
    "Filename": "sub-123_ses-01_fieldmap.nii.gz",
    "Folder": "fmap",
    "ignore": false,
    "IntendedFor": [
      {
        "Folder": "func"
      }
    ],
    "Modality": "fieldmap",
    "Path": "sub-123/ses-01/fmap",
    "Run": "",
    "template": "fieldmap_file",
    "valid": true
  },
  "IntendedFor": [
    "ses-01/func/sub-123_ses-01_task-stroop_run-02_bold.nii.gz",
    "ses-01/func/sub-123_ses-01_task-stroop_run-01_bold.nii.gz"
  ],
  "PhaseEncodingDirection": "j-",
  "TotalReadoutTime": 0.123
}
```

- Note: `PhaseEncodingDirection` and `TotalReadoutTime` are typically required.

2. Functional ([BIDS specification](https://bids-specification.readthedocs.io/en/stable/04-modality-specific-files/01-magnetic-resonance-imaging-data.html#task-including-resting-state-imaging-data))

- Each functional NIfTI needs the following metadata: `EchoTime`, `EffectiveEchoSpacing`, `PhaseEncodingDirection`,`RepetitionTime`, `SliceTiming`, and `TaskName`.

#### Prerequisite Metadata

1. BIDS

   - {container}.info.BIDS
   - Enables `export_bids` to download the proper data

2. Depending upon your fMRIPrep workflow preferences, a variety of metadata and files may be required for successful execution. And because of this variation, not all cases will be caught during BIDS validation. If you are running into issues executing bids-fmriprep, we recommend reading through the configuration options explained with the [fMRIPrep Usage Notes](https://fmriprep.org/en/stable/usage.html) and double-checking that the [required files](#prerequisite-files) follow the specifications.

## Usage

Flywheel BIDS App gears allow the user to provide the commandline input that they would normally run for the BIDS app. The BIDS App Toolkit parses any arguments beyond the typical `fmriprep BIDS_dir output_dir analysis_level` when creating the BIDSAppContext object (see flywheel_bids.flywheel_bids_app_toolkit.context.parse_bids_app_commands).

### Setup:

Before running BIDS curation on your data, you must first prepare your data with the following steps:

1. Run the [file-metadata-importer](https://gitlab.com/flywheel-io/scientific-solutions/gears/file-metadata-importer) gear on all the acquisitions in your dataset
   - This step extracts the DICOM header info and stores it as Flywheel Metadata.
1. Run the [file-classifier](https://gitlab.com/flywheel-io/scientific-solutions/gears/file-classifier) gear on all the acquisitions in your dataset
   - This step sets [classification metadata](https://docs.flywheel.io/user/enhance/user_using_custom_project_info_to_define_custom_classifications/#instruction-steps).
1. Run the [DCM2NIIX: DICOM to NIfTI converter](https://gitlab.com/flywheel-io/scientific-solutions/gears/dcm2niix) gear on all the acquisitions in your dataset
   - This step generates the NIfTI files that fMRIPrep needs from the DICOMS. It also copies all flywheel metadata from the DICOM to the NIfTI file (In this case, all the DICOM header information we extracted in step 1)
1. Run the [curate-bids gear](https://gitlab.com/flywheel-io/flywheel-apps/curate-bids) on the project. More information about BIDS Curation on Flywheel can be found [here](https://docs.flywheel.io/user/bids/user_bids_getting_started/) and running the BIDS curation gear is described [here](https://docs.flywheel.io/Developer_Guides/dev_bids_curation_3_curation_gear_v2/). If you need to rename sessions or subjects before curation, you may find the gear helpful: [relabel-container](https://gitlab.com/flywheel-io/scientific-solutions/gears/relabel-container).

1. Run fMRIPrep on a session, subject, or project.

Steps 1 through 3 can be automatically carried out as [gear rules](https://docs.flywheel.io/hc/en-us/articles/360008553133-Project-Gear-Rules).

These steps MUST be done in this order. NIfTI file headers have significantly fewer fields than the DICOM headers. File metadata will be written to .json sidecars when the files are exported in the BIDS format as expected by the fMRIPrep BIDS App which is run by this gear.

### Running:

To run the gear, [session](https://docs.flywheel.io/user/compute/gears/user_analysis_gears/) or [subject](https://docs.flywheel.io/user/compute/gears/user_running_an_analysis_gear_on_a_subject/). See the instructions for running the BIDS curation gear [at different levels here](https://docs.flywheel.io/Developer_Guides/dev_bids_curation_3_curation_gear_v2/#curating-a-session).

If you run this gear on an entire project, it will take a very long time and likely lead to a disk full error because it will sequentially step through each subject. Instead, you can launch multiple bids-fmriprep jobs on subjects or sessions in parallel. An example of running bids-fmriprep on a subject using the Flywheel SDK is in this [notebook](./notebooks/run-bids-fmriprep.ipynb). More details about running gears using the SDK can be found in this [tutorial](https://docs.flywheel.io/user/compute/gears/user_running_gears_as_a_batch/).

Note that bids-fmriprep will take a _long_ time to run because it runs Freesurfer. Depending on the number of structural and functional files, it can run for 12 to 48 or more hours.

### Workflow

```mermaid
graph LR;
    A[default fMRIPrep command]:::input --> D[Parser];
    B[optional fMRIPrep command]:::input --> D;
    C[Configuration<br>options]:::input --> D;
    D:::parser --> E((fMRIPrep));
    E:::gear --> F[HTML reports]:::parser;

    classDef parser fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff

```

Description of workflow

1. Select and supply configuration options
1. Gear parses and cleans options and configurations for `fMRIPrep`
1. Gear uploads HTML reports

### Logging

Debug is the highest number of messages and will report some locations/options and all errors. If unchecked in the configuration tab, only INFO and higher priority level log messages will be reported.

### Resources

fMRIPrep can require a large amount of memory and disk space depending on the number of acquisitions being analyzed. There is also a trade-off between the cost of analysis and the amount of time necessary. There is a helpful discussion of this in the [FAQ](https://fmriprep.org/en/20.2.6/faq.html#how-much-cpu-time-and-ram-should-i-allocate-for-a-typical-fmriprep-run) and also on [NeuroStars](https://neurostars.org/t/how-much-ram-cpus-is-reasonable-to-run-pipelines-like-fmriprep/1086). At the top of your job log, you should see the configuration of the virtual machine you are running on. When a job finishes, the output of the GNU `time` command is placed into the "Custom Information" (metadata) on the analysis. To see it, go to the "Analyses" tab for a project, subject, or session, click on an analysis and then on the "Custom Information" tab.

## FAQ

[FAQ.md](FAQ.md)

## Contributing

[For more information about how to get started contributing to this gear,
check out [CONTRIBUTING.md](CONTRIBUTING.md).]

<!-- markdownlint-disable-file -->
