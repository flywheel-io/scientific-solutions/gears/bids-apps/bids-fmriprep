"""Module to test providing input files"""

from os import chdir
from pathlib import Path
from unittest.mock import MagicMock, patch

import flywheel
from flywheel_gear_toolkit import GearToolkitContext

from run import main
from tests.utils_for_testing import change_path_for_testing

FWV0 = Path.cwd()
TEST_DATA_DIR = "tests/integration_tests/gears"


@patch("fw_gear_bids_fmriprep.utils.helpers.validate_kwargs")
@patch("fw_gear_bids_fmriprep.main.install_freesurfer_license")
@patch("fw_gear_bids_fmriprep.main.get_bids_data")
@patch("run.run_bids_algo")
@patch("run.save_metadata")
def test_load_inputs_with_inputs_works(
    mock_save_metadata,
    mock_run_bids_algo,
    mock_get_bids_data,
    mock_install_freesurfer_license,
    mock_validate_kwargs,
    sdk_mock,
    capfd,
    print_captured,
    search_log_contains,
):
    """Test main with inputs in the config.json file.

    This test simulates a running gear by using the config.json file, and the input,
    output and work directories in a "running gear directory" that is a subdirectory
    of the tests/gears directory.  To do this, the current working directory is changed
    to the running gear directory and this requires that the path to config.json and
    the paths to the inputs in config.json need to be changed to be found relative to
    the running gear directory.  After the tests, the current working directory is
    restored to the original directory."""

    chdir(FWV0 / TEST_DATA_DIR / "inputs")  # get inside the simulated running gear directory
    config_path = Path().cwd()
    config_file = config_path / "config.json"
    with GearToolkitContext(config_path=config_file, input_args=list()) as gear_context:
        gear_context.init_logging()

        # Sets the return value for this line
        #   destination = gear_context.client.get(gear_context.destination["id"])
        # which is in
        #   flywheel_bids_app_toolkit.utils.query_flywheel.get_fw_details()
        sdk_mock.get.return_value = flywheel.Analysis(
            label="test",
            parent=flywheel.Subject(id="aex", type="subject"),
            parents={"group": "scien", "project": "someproj", "subject": "sub-003"},
        )
        change_path_for_testing(gear_context, config_path)

        mock_get_bids_data.return_value = (MagicMock(), [])

        _ = main(gear_context)

    mock_validate_kwargs.assert_called_once()
    mock_install_freesurfer_license.assert_called_once()
    mock_get_bids_data.assert_called_once()
    mock_run_bids_algo.assert_called_once()
    mock_save_metadata.asser_called_once()

    captured = capfd.readouterr()
    print_captured(captured)

    assert search_log_contains(captured.out, "Log level is", "INFO")
    assert search_log_contains(captured.out, "UPDATED command is", "bids_filter_file.json")
    # Mocking prevents the creation of the output directory which is the analysis container ID
    # so this shows up as an error in the log
    assert search_log_contains(captured.err, "Path NOT found", "output/6622d9e7f8f4d5907240c5ea")

    chdir(FWV0)  # get back to where we once belonged
