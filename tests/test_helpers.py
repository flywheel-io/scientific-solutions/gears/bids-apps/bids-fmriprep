from pathlib import Path
from unittest.mock import Mock, patch

import pytest

from fw_gear_bids_fmriprep.utils.helpers import (
    pare_down_files,
    reconcile_existing_and_unzipped_files,
    reset_FS_subj_paths,
    validate_setup,
)


@pytest.mark.parametrize("keep_fsaverage", [True, False])
def test_pare_down_files(keep_fsaverage):
    gear_context = Mock()
    gear_context.config.get.return_value = keep_fsaverage
    app_context = Mock()
    app_context.analysis_output_dir = "/fake/analysis/output/dir"

    with patch("fw_gear_bids_fmriprep.utils.helpers.shutil.rmtree") as mock_rmtree:
        with patch(
            "fw_gear_bids_fmriprep.utils.helpers.Path.glob",
            return_value=[Path("/fake/analysis/output/dir/freesurfer/fsaverage")],
        ):
            pare_down_files(gear_context, app_context)
            if keep_fsaverage:
                mock_rmtree.assert_not_called()
            else:
                mock_rmtree.assert_called_once()


def test_reconcile_existing_and_unzipped_files():
    target_dir = Path("/fake/target/dir")
    unzip_dir = Path("/fake/unzip/dir")

    with patch("fw_gear_bids_fmriprep.utils.helpers.Path.glob", return_value=[Path("/fake/unzip/dir/file1")]):
        with patch("fw_gear_bids_fmriprep.utils.helpers.shutil.move") as mock_move:
            with patch("fw_gear_bids_fmriprep.utils.helpers.Path.exists", side_effect=[False, True]):
                reconcile_existing_and_unzipped_files(target_dir, unzip_dir)
                mock_move.assert_called_once_with(Path("/fake/unzip/dir/file1"), target_dir / "file1")


def test_reset_FS_subj_paths():
    gear_context = Mock()
    gear_context.work_dir = "/fake/work/dir"

    with patch("fw_gear_bids_fmriprep.utils.helpers.Path.mkdir") as mock_mkdir:
        with patch("fw_gear_bids_fmriprep.utils.helpers.Path.symlink_to") as mock_symlink_to:
            reset_FS_subj_paths(gear_context)
            mock_mkdir.assert_called_once()
            assert mock_symlink_to.call_count == 3


@patch("fw_gear_bids_fmriprep.utils.helpers.validate_kwargs")
def test_validate_setup(mock_validate_kwargs, mock_context, mock_app_context):
    mock_app_context.bids_app_options = {"oooo": "pick_me"}
    validate_setup(mock_app_context)
    mock_validate_kwargs.assert_called_once_with(mock_app_context)
